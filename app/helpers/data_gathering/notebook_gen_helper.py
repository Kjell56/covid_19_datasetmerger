import nbconvert
import nbformat

from app.helpers.database.upload_data import UploadData


def create_html_from_notebook(path, file):
    """
    Function for creating html from notebooks automatically
    path:
        The path of the notebook
    file:
        The specific file of the notebook
    :return:
    """
    exporter = nbconvert.HTMLExporter()
    # exporter.template_file = 'basic'
    nb = nbformat.read(path + file + ".ipynb", 4)
    html, resources = exporter.from_notebook_node(nb)

    return html
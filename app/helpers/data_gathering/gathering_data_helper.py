import time

from app.helpers import logger_helper
from app.services.kaggle_data.kaggle_data import KaggleData
from app.helpers.unpack_zip import Unpack
from app.services.git_data.git_data import GITData
from app.services.carbon_emissions.carbon_emissions import CO2Signal
from app.services.electricity_usage.electricity_service import ElectricityService

def download_kaggle_files():
    """
    Main function for downloading Kaggle files
    """
    logger_helper.logger("downloading Kaggle files...", 'info', 'data_center')

    # Create new kaggle data, download some datasets and unpack into the Kaggle directory
    kaggle_data = KaggleData()
    kaggle_data.download_comp_dataset("covid19-global-forecasting-week-5", "data/kaggle/comp_data")

    logger_helper.logger("unpacking Kaggle ZIP file", 'info', 'data_center')
    Unpack("data/kaggle")
    Unpack("data/kaggle/comp_data")


def download_git_files():
    """
    Main function for downloading GIT files
    """
    logger_helper.logger("downloading git files", 'info', 'data_center')
    # sand_data = GITData("CSSEGISandData", "COVID-19", "")
    # sand_data.download_all_data()
    corana_NL = GITData("J535D165", "CoronaWatchNL", "data", 1)  # First the username, repo and the data directory
    corana_NL.download_all_data()


def carbon_emissions_service():
    """
    Main function for downloading CO2 emission
    """
    logger_helper.logger("downloading emission files", 'info', 'data_center')
    co2 = CO2Signal()
    co2.get_from_country("NL")


def electricity_service_generation():
    """
    Main function for downloading electricity generation data, especially wind and solar energy
    """
    logger_helper.logger("downloading electricity usage files...", 'info', 'data_center')

    # Get token and insert into new electricity service
    SECURITY_TOKEN = "4101a26a-7347-4920-b755-8cf6f8d55de2"
    elec = ElectricityService(SECURITY_TOKEN)

    ym = time.strftime("%Y%m%d")  # get current date

    netherlands = elec.get_solar_energy("20200401", ym, 'Europe/Amsterdam', 'NL')
    df = elec.aggregate_data(netherlands, 'd')
    generation_daily_data = elec.reformat_data(df)

    return generation_daily_data


def electricity_service_load():
    """
    Main function for gathering electricity load data. Now this is done for both Dutch and Belgium data
    from a specific date
    """
    logger_helper.logger("downloading electricity usage files...", 'info', 'data_center')

    # Get token and insert into new electricity service
    SECURITY_TOKEN = "4101a26a-7347-4920-b755-8cf6f8d55de2"
    elec = ElectricityService(SECURITY_TOKEN)

    ym = time.strftime("%Y%m%d")  # get current date

    netherlands = elec.get_load_data("20200401", ym, 'Europe/Amsterdam', 'NL')
    belgium = elec.get_load_data("20200401", ym, 'Europe/Amsterdam', 'BE')

    # get current day
    # netherlands = elec.get_current_load('Europe/Amsterdam', 'NL', ym)
    # belgium = elec.get_current_load('Europe/Amsterdam', 'BE', ym)

    # aggregate and merge
    df = elec.aggregate_data(netherlands, 'd')
    df2 = elec.aggregate_data(belgium, 'd')
    aggregated_merged_df = elec.merge_data(df, df2)
    aggregated_merged_df = elec.reformat_data(aggregated_merged_df)
    elec.save_data(aggregated_merged_df, "daily_aggregated_electricity")

    # aggregate to hours
    df = elec.aggregate_data(netherlands, 'h')
    df2 = elec.aggregate_data(belgium, 'h')
    non_aggregated_merged_df = elec.merge_data(df, df2)
    non_aggregated_merged_df = elec.reformat_data(non_aggregated_merged_df)
    elec.save_data(non_aggregated_merged_df, "hourly_aggregated_electricity")

    return aggregated_merged_df, non_aggregated_merged_df

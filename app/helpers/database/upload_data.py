import json
import time

import pymongo
from pymongo.errors import BulkWriteError, DuplicateKeyError
import gridfs
from app.helpers import logger_helper


class UploadData:
    """
    Class for uploading all kinds of data to MongoDB. For example, only a single entry or saving all entries
    """
    def __init__(self, database, collection):
        """
        Database, client and collection initiation
        database:
            Database in mongodb
        collection:
            Collection in mongodb
        """
        self.database = database
        self.collection = collection
        self.client = pymongo.MongoClient(
            "mongodb+srv://kjell:Creative01@cluster0-hsrlj.mongodb.net/test?retryWrites=true&w=majority")

        # Try to make connection to the database and collection
        try:
            self.db = self.client[self.database]
            self.cursor = self.db[self.collection]
            logger_helper.logger("Successfully connected to server", "info", "merger")
        except pymongo.errors.ConnectionFailure as e:
            logger_helper.logger("Could not connect to server: %s" % e, "error", "merger")

    def save_single_entry(self, single_entry):
        """
        Saving a single entry will means one document
        single_entry:
            The document to save
        """
        try:
            single_entry = self.__prepare_data(single_entry, True)
            self.cursor.insert(single_entry)
        except DuplicateKeyError as e:
            logger_helper.logger("Could not upload entry %s" % e.details, "warning", "merger")

    def save_all_entries(self, bulk_data):
        """
        Will save a bulk of data, thus, multiple documents at once
        bulk_data:
            list of json data
        """
        try:
            bulk_data = self.__prepare_data(bulk_data, False)
            self.cursor.insert_many(bulk_data, ordered=False)
        except BulkWriteError as e:
            logger_helper.logger("Could not upload all data: %s" % e.details['writeErrors'], "warning", "merger")

    def save_file(self, file, name):
        """
        Will save a file instead of a document with GRIDFS. In this case it will save html files
        file:
            The html file
        name:
            Naming the html file to get from database later
        """
        fs = gridfs.GridFS(self.db)

        # first delete old file
        def delete_html():
            try:
                old = fs.get_last_version(filename=name)
                fs.delete(old._id)
                logger_helper.logger("file deleted", "info", "merger")
            except:
                logger_helper.logger("Could not delete file", "error", "merger")

        # Finally, try to update html
        def update_html(ym, htmlID, meta):
            try:
                logger_helper.logger("Updating HTML in db", "info", "merger")
                new_values = {
                    "$set": {
                        'date': ym,
                        'html': htmlID
                    }
                }
                self.cursor.update({"_id": meta['_id']}, new_values)
                logger_helper.logger("HTML in db updated", "info", "merger")

            except DuplicateKeyError as e:
                logger_helper.logger("Could not upload entry %s" % e.details, "warning", "merger")

        # Then try to insert new html
        def insert_html():
            htmlID = fs.put(file, encoding='utf-8', filename=name)
            ym = time.strftime("%Y-%m-%d")  # get current date

            # Put meta data
            meta = {
                '_id': name,
                'date': ym,
                'html': htmlID
            }

            try:
                self.cursor.insert_one(meta)
                logger_helper.logger("Now sending HTML file to db...", "info", "merger")
            except DuplicateKeyError as e:
                logger_helper.logger("Could not upload entry %s" % e.details, "warning", "merger")
                update_html(ym, htmlID, meta)

        delete_html()
        insert_html()

    @staticmethod
    def __prepare_data(df, tail):
        """
        Helper for only upload last entry when merging cannot be done because of trailing entries
        df:
            The dataframe (pandas)
        tail:
            Check if we need it
        """
        df.reset_index(inplace=True)

        # Only save last entry right now
        # TODO: do not download all data for efficiency
        if tail:
            df = df.tail(1)

        json_data = json.loads(df.T.to_json()).values()

        return json_data

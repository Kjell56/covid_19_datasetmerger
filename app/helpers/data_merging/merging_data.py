from app.helpers import logger_helper
from app.services.merge_datasets import merge_comp_dataset
from app.services.merge_datasets.merge_dutch_datasets import MergeDutchDatasets


def merge_datasets_service(electricity_daily_df):
    """
    Main function for merging 2 different datasets together.
    In this case this is CoronaNL and electricity data
    electricity_daily_df:
        Electricity dataframe
    """
    logger_helper.logger("Merging files...", 'info', 'merger')

    # Merge Dutch datasets
    merge_dutch_dataset = MergeDutchDatasets('data/git/CoronaWatchNL')
    merged_df_stand = merge_dutch_dataset.merge_standard_dutch_info(['rivm_corona_in_nl_daily.csv',
                                                                     'rivm_corona_in_nl_fatalities.csv',
                                                                     'rivm_corona_in_nl_hosp.csv'])
    # merge_dutch_dataset.merge_dutch_non_standard(merged_df_stand, ["rivm_NL_covid19_sex.csv",
    #                                                                "rivm_NL_covid19_age.csv"])

    merge_datasets_electricity = MergeDutchDatasets('data/electricity_usage')
    dutch_electricity = merge_datasets_electricity. \
        merge_dutch_electricity(merged_df_stand, electricity_daily_df)

    return dutch_electricity


def merge_timeseries():
    """
    Main function for merging all timeseries together
    :return:
    """
    # Open and merge the data from timeseries
    data, data_age, data_pop = merge_comp_dataset.read_rename_data()
    data, data_age, data_pop = merge_comp_dataset.clean_data(data, data_age, data_pop)
    data = merge_comp_dataset.merge_data(data, data_age, data_pop)

    return data


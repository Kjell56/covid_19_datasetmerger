import errno
import glob, os
from app.helpers import logger_helper
from zipfile import ZipFile


class Unpack:
    """
    Class for unpacking files within directory
    """

    def __init__(self, dir):
        """
        Initate the directory and unpack all files
        dir:
            The directory to be unpacked
        """
        self.dir = dir
        self.__unpack()

    def __unpack(self):
        """
        Unpack all files. Will first go into the file list and then unpack per list entry
        :return:
        """
        file_list = self.__get_file_list()

        for i in file_list:
            with ZipFile(i, 'r') as zipObj:
                logger_helper.logger("unpacking contents", 'info', 'unpack')
                new_dir = i.replace('.zip', '')  # Create new directory with same file name
                self.__create_dir(new_dir)

                zipObj.extractall(new_dir)  # Extract all the contents of zip file in current directory

                self.__remove_file(i)  # Remove ZIP

    def __get_file_list(self):
        """
        Get all files from directory
        :return:
        """

        file_list = []
        for zip_file in glob.glob(self.dir + "/*zip"):
            file_list.append(zip_file)

        return file_list

    @staticmethod
    def __create_dir(local_dir):
        """
        Create a new directory if required
        local_dir:
            Directory name
        :return:
        """
        try:
            os.makedirs(local_dir + "/")
            logger_helper.logger("directory: " + local_dir + "/ created", 'info', 'unpack')
        except OSError as e:
            if e.errno != errno.EEXIST:
                logger_helper.logger("file already exists", 'info', 'unpack')
                raise

    @staticmethod
    def __remove_file(file):
        """
        Remove old file if required
        file:
            File to be deleted
        :return:
        """
        try:
            os.remove(file)
            logger_helper.logger("file: " + file + "removed", 'info', 'unpack')
        except:
            logger_helper.logger("Error while deleting file " + file, 'error', 'unpack')

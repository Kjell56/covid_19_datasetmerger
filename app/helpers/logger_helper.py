import logging


def setup_logger(logger_name, log_file, level=logging.INFO):
    """
    Will setup the logger and add handler for logging

    logger_name:
        name of the logger, will be used for logger function
    log_file:
        where log file will be saved
    level:
        info, error etc
    """
    log_setup = logging.getLogger(logger_name)
    formatter = logging.Formatter('%(levelname)s: %(asctime)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')
    fileHandler = logging.FileHandler(log_file, mode='a')
    fileHandler.setFormatter(formatter)
    streamHandler = logging.StreamHandler()
    streamHandler.setFormatter(formatter)
    log_setup.setLevel(level)
    log_setup.addHandler(fileHandler)
    log_setup.addHandler(streamHandler)


def logger(msg, level, logfile):
    """
    Here the logger can be called for inserting new logs. Please make new if statement with new log file if required
    msg:
        The message you want to give to the logger
    level:
        Error, info etc
    logfile:
        The file required for logging
    """
    global log
    if logfile == 'data_center':
        log = logging.getLogger('data_center')
    if logfile == 'unpack':
        log = logging.getLogger('unpack')
    if logfile == 'electricity':
        log = logging.getLogger('electricity')
    if logfile == 'kaggle':
        log = logging.getLogger('kaggle')
    if logfile == 'carbon':
        log = logging.getLogger('carbon')
    if logfile == 'git':
        log = logging.getLogger('git')
    if logfile == 'merger':
        log = logging.getLogger('merger')
    if level == 'info':
        log.info(msg)
    if level == 'warning':
        log.warning(msg)
    if level == 'error':
        log.error(msg)

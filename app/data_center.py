from app.helpers import logger_helper
from app.helpers.data_merging.merging_data import merge_timeseries, merge_datasets_service
from app.helpers.database.upload_data import UploadData

import time

from app.helpers.data_gathering.gathering_data_helper import download_kaggle_files, download_git_files, \
    electricity_service_load, electricity_service_generation
from app.helpers.data_gathering.notebook_gen_helper import create_html_from_notebook


def setup_logger():
    """
    Function for setting up logger, needs function, log path and timestamp
    :return:
    """
    logger_helper.setup_logger("data_center", "logs/data_center/data_center_" + ym + ".log")
    logger_helper.setup_logger("unpack", "logs/unpacking_helper/unpacking_helper_" + ym + ".log")
    logger_helper.setup_logger("git", "logs/git/git_" + ym + ".log")
    logger_helper.setup_logger("kaggle", "logs/kaggle/kaggle_" + ym + ".log")
    logger_helper.setup_logger("electricity", "logs/electricity/electricity_" + ym + ".log")
    logger_helper.setup_logger("carbon", "logs/carbon/carbon_" + ym + ".log")
    logger_helper.setup_logger("merger", "logs/merger/merger_" + ym + ".log")


def upload_data(merged_data, hourly_data, daily_data, generation_daily_data, timeseries_data):
    """
    Wrapper function for uploading all data to mongodb
    merged_data:
        The merged data from CoronaNL and electricity data
    hourly_data:
        The hourly data from electricity load
    daily_data:
        The daily data from electricity load
    generation_daily_data:
        The daily data for electricity generation
    timeseries_data:
        The data from Paul's analysis
    """
    upload_merged_dutch_electricity = UploadData("merger_db", "Dutch")
    upload_merged_dutch_electricity.save_all_entries(merged_data)

    upload_non_merged_daily_electricity = UploadData("merger_db", "electricity_hourly_dutch")
    upload_non_merged_daily_electricity.save_all_entries(hourly_data)

    upload_aggregated_data = UploadData("merger_db", "electricity_daily_dutch")
    upload_aggregated_data.save_all_entries(daily_data)

    upload_aggregated_data = UploadData("merger_db", "electricity_generation_daily")
    upload_aggregated_data.save_all_entries(generation_daily_data)

    upload_data = UploadData("merger_db", "covid_19_timeseries")
    upload_data.save_all_entries(timeseries_data)


def create_upload_html():
    """
    Wrapper function for creating html notebooks and uploading all data to Mongodb
    """
    upload_html = UploadData("merger_db", "html_files")
    html = create_html_from_notebook("app/services/electricity_usage/",
                                     "Context of energy consumption consumer and business")
    upload_html.save_file(html, "context_energy_consumption")

    html = create_html_from_notebook("app/services/electricity_usage/", "Analyze Solar Wind Energy")
    upload_html.save_file(html, "solar_wind_analysis")

    html = create_html_from_notebook("app/services/electricity_usage/", " analyze_electricity_data")
    upload_html.save_file(html, "electricity_analysis")


if __name__ == '__main__':
    ym = time.strftime("%Y%m%d")  # get current date

    # Setting up loggers
    setup_logger()

    # Downloading data from services
    download_kaggle_files()
    download_git_files()
    # carbon_emissions_service()

    # Process data
    timeseries_data = merge_timeseries()
    daily_data, hourly_data = electricity_service_load()
    generation_daily_data = electricity_service_generation()
    merged_data = merge_datasets_service(daily_data)

    # Upload the data
    upload_data(merged_data, hourly_data, daily_data, generation_daily_data, timeseries_data)

    # Create html from notebooks
    create_upload_html()

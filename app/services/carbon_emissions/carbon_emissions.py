import calendar;
from app.helpers import logger_helper
import time
import json
import requests
import os


class CO2Signal:
    """
    Class for gathering CO2 entries from API
    """
    headers = {'auth-token': '44fa11d61d4d5d69'}

    def get_from_country(self, country_code):
        """
        Get the current CO2 from country
        country_code:
            Country to be extracted from
        :return:
        """
        logger_helper.logger("gathering data from country: " + country_code, 'info', 'carbon')
        data = requests.get('https://api.co2signal.com/v1/latest?countryCode=' + country_code,
                            headers=CO2Signal.headers)
        if data:
            logger_helper.logger("data gathered successfully", 'info', 'carbon')
        else:
            logger_helper.logger("data could not be downloaded...", 'error', 'carbon')
        decoded_data = data.json()
        ts = calendar.timegm(time.gmtime())
        self.__create_json(decoded_data['data'], ts)

    @staticmethod
    def __create_json(data, ts):
        """
        Create a json file per entry
        data:
            The data to be added
        ts:
            The current timestamp of the data
        :return:
        """
        logger_helper.logger("creating new data with json", 'info', 'carbon')
        data_set = {"timestamp": ts, "data": [{"carbonIntensity": data["carbonIntensity"],
                                               "fossilFuelPercentage": data['fossilFuelPercentage']}]}

        json_dump = json.dumps(data_set)
        file_url = "data/co2/co2_emissions_" + str(ts) + ".json"
        with open(file_url, 'w') as file:
            json.dump(json_dump, file)

        if os.path.isfile(file_url):
            logger_helper.logger("file successfully saved", 'info', 'carbon')
        else:
            logger_helper.logger("File could not be saved...", 'error', 'carbon')

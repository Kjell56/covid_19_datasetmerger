from datetime import datetime

from deprecated import deprecated

from app.helpers import logger_helper
import pandas as pd
import os
import matplotlib.pyplot as plt
from entsoe import EntsoePandasClient


class ElectricityService:
    """
    Service for gathering the electricity data from Entsoe
    """
    def __init__(self, security_token):
        """
        Make a pandas connection with api key
        security_token:
            Token which was supplied by Entsoe
        """
        self.SECURITY_TOKEN = security_token
        self.client = EntsoePandasClient(api_key=security_token)

    def get_load_data(self, start, stop, timezone, country_code):
        """
        Get all data regarding the load between a certain time period for a country
        start:
            Timestamp start
        stop:
            Timestamp stop
        timezone:
            Timezone country
        country_code:
            Country code
        :return:
        """
        logger_helper.logger("loading electricity service data", 'info', 'electricity')
        start = pd.Timestamp(start, tz=timezone)
        end = pd.Timestamp(stop, tz=timezone)
        data = self.client.query_load(country_code, start=start, end=end)
        if isinstance(data, pd.Series):
            data = pd.DataFrame({'time': data.index, 'load': data.values})
        if data.isna:
            logger_helper.logger("data loaded", 'info', 'electricity')
        else:
            logger_helper.logger("something went wrong while loading the data...", 'error', 'electricity')
        return data

    def get_solar_energy(self, start, stop, timezone, country_code):
        """
        Same as load, but now for solar and wind energy generation
          start:
            Timestamp start
        stop:
            Timestamp stop
        timezone:
            Timezone country
        country_code:
            Country code
        :return:
        """
        logger_helper.logger("loading electricity generation data", "info", "electricity")
        start = pd.Timestamp(start, tz=timezone)
        end = pd.Timestamp(stop, tz=timezone)
        data = self.client.query_wind_and_solar_forecast(country_code, start=start, end=end)
        if isinstance(data, pd.Series):
            data = pd.DataFrame({'time': data.index, 'load': data.values})
        if data.isna:
            logger_helper.logger("data loaded", 'info', 'electricity')
        else:
            logger_helper.logger("something went wrong while loading the data...", 'error', 'electricity')
        return data

    def get_current_load(self, timezone, country_code, today):
        """
        Gathering current load for today
        timezone:
            Timezone country
        country_code:
            Country code
        today:
            Current day timestamp
        :return:
        """
        logger_helper.logger("loading electricity service data from current day", 'info', 'electricity')
        start = pd.Timestamp(today, tz=timezone)
        end = pd.Timestamp(today, tz=timezone)
        data = self.client.query_load(country_code, start=start, end=end)
        if isinstance(data, pd.Series):
            data = pd.DataFrame({'time': data.index, 'load': data.values})
        if data.isna:
            logger_helper.logger("data loaded", 'info', 'electricity')
        else:
            logger_helper.logger("something went wrong while loading the data...", 'error', 'electricity')
        return data

    @staticmethod
    def merge_data(df1, df2):
        """
        FOr merging dutch and belgium data together
        df1:
            Dutch data
        df2:
            Belgium data
        :return:
        """
        df1 = df1.rename(columns={"load": "load_dutch"})
        df2 = df2.rename(columns={"load": "load_belgium"})

        new_df = pd.merge(df1, df2, left_index=True, right_index=True)

        return new_df

    @staticmethod
    def reformat_data(df_electricity):
        """
        Function for reformatting data to correct timestamp etc
        TODO: need better pipelining
        :param df_electricity:
        :return:
        """
        df_electricity['time'] = df_electricity.index.to_frame()
        df_electricity_new = df_electricity.rename(columns={"time": "electricity_load_date"})
        df_electricity_new['electricity_load_date'] = pd.to_datetime(df_electricity_new['electricity_load_date'],
                                                                     utc=True)  # convert to datetime
        df_electricity_new['electricity_load_date'] = df_electricity_new['electricity_load_date'].dt.tz_convert(
            "Europe/Amsterdam")  # convert to correct Summer time
        df_electricity_new['time'] = df_electricity_new['electricity_load_date'].dt.strftime(
            '%Y-%m-%d %H:%M:%S')  # keep timely based data
        df_electricity_new = df_electricity_new.set_index("time")
        df_electricity_new['_id'] = df_electricity_new.index
        df_electricity_new = df_electricity_new.set_index("_id")

        df_electricity_new['electricity_load_date'] = df_electricity_new['electricity_load_date'].dt.date
        df_electricity_new['electricity_load_date'] = pd.to_datetime(df_electricity_new['electricity_load_date'])
        df_electricity_new['date'] = df_electricity_new['electricity_load_date'].astype(str)
        return df_electricity_new

    @staticmethod
    @deprecated
    def save_data(df, filename):
        """
        Function for saving the electricity data to a file
        :param df:
        :param filename:
        :return:
        """
        logger_helper.logger("saving data...", 'info', 'electricity')
        now = datetime.now()
        timestamp = datetime.timestamp(now)
        url = "data/electricity_usage/" + filename + ".csv"  # str(timestamp) + ".csv"
        df.to_csv(url)
        if os.path.isfile(url):
            logger_helper.logger("file successfully saved", 'info', 'electricity')
        else:
            logger_helper.logger("File could not be saved", 'error', 'electricity')

    # Aggregate the data based on some date type and remove nans
    @staticmethod
    def aggregate_data(df, type):
        """
        Function for aggregating data to a certain type such as day or year
        df:
            The data
        type:
            Type of aggregation
        :return:
        """
        if 'time' not in df.columns:
            df["time"] = df.index

        logger_helper.logger("Aggregating data...", 'info', 'electricity')
        cols = df.columns.difference(['time'])

        # if possible convert to float
        df[cols] = df[cols].astype(float)

        df = df.resample(type, on='time').mean()
        df = df.dropna()

        return df

    @staticmethod
    @deprecated()
    def plot_data(df):
        df.plot()
        plt.show()

from pyspark.sql.functions import lit, flatten, from_json, col
from pyspark.sql.types import StructType, StructField, StringType, DoubleType, ArrayType
import json
from pyspark.sql import Row
import glob

from pyspark import SparkConf, SparkContext
from pyspark.sql import *
import numpy as np
spark = SparkSession.builder.appName("PySpark Appaa").master("spark://spark-master:7077").config("spark.sql.repl.eagerEval.enabled", "true").getOrCreate()
sc = spark.sparkContext

contents = None
lastitem = None
data = None
def distributedJsonRead(s3Key):
    with open(s3Key, 'r') as myfile:
        data=myfile.read()
    lastitem = s3Key
    contents = json.loads(data)
    for dicts in contents['data']:
        yield Row(**dicts)

keyList = glob.glob('/home/jovyan/notebooks/s3_files/*00-airquality-extract.json')
pkeys = sc.parallelize(keyList) #keyList is a list of s3 keys
dataRdd = pkeys.flatMap(distributedJsonRead)
df = dataRdd.toDF()
final = df.select(['formula','station_number','timestamp_measured','value'])
#final.write.format("csv").save('/home/jovyan/notebooks/measures_locations_part2.csv')
final.coalesce(1).write.csv('/home/jovyan/notebooks/measures_locations_part7.csv')

import pandas as pd
import plotly.express as px
import pymongo

MONGODB_URI = '<FILL IN HERE>' # format mongodb+srv://[USERNAME]:[PASSWORD]@[CLUSTER].mongodb.net/test?retryWrites=true&w=majority

client = pymongo.MongoClient(MONGODB_URI)
db = client.airquality
collection = db.measures
elements = collection.aggregate([{"$match": {"timestamp_measured": {"$gt": "2019-01-01T01:00:00+00:00"}, "formula": "NO"}}])
measures = pd.DataFrame(list(elements))

df = measures[['timestamp_measured', 'value', 'formula', 'station_number']]
df['timestamp_measured'] = pd.to_datetime(df['timestamp_measured'], format='%Y-%m-%dT%H:%M:%S+00:00').dt.strftime("%d-%m-%Y %H:%M")

df2 = df
df = df[df['station_number'] == 'NL10639']
df = df.drop_duplicates()
df = df.dropna()

px.line(df, x="timestamp_measured", y="value").show()

import logging
import requests
import json
import pandas as pd
from datetime import datetime, timedelta

logging.basicConfig(filename='./api_logs.log', level=logging.DEBUG)

MAX_DATE = "2019-01-02T01:00:00"

DATA_PATH = './data/'
ENDPOINT_BASE = 'https://api.luchtmeetnet.nl/open_api'

def handler(event, context):
    begin_date = datetime.strptime(event['date_last'], '%Y-%m-%dT%H:%M:%S')
    end_date = datetime.strptime(event['date_last'], '%Y-%m-%dT%H:%M:%S') + timedelta(days=1)
    print(event)

    while end_date < datetime.strptime(MAX_DATE, '%Y-%m-%dT%H:%M:%S'):
        data = extract_airquality_data(begin_date.strftime('%Y-%m-%dT%H:%M:%S'), end_date.strftime('%Y-%m-%dT%H:%M:%S'))

        try:
            START_DATE = begin_date.strftime('%Y-%m-%dT%H:%M:%S')
            END_DATE = end_date.strftime('%Y-%m-%dT%H:%M:%S')
            FILE_NAME = f'{DATA_PATH}{START_DATE}-{END_DATE}-airquality-extract.json'
            with open(FILE_NAME, 'w') as file:
                json.dump(data, file)
            print(f'Written file {FILE_NAME}')
        except Exception as e:
            raise IOError(e)

        begin_date = begin_date + timedelta(days=1)
        end_date = end_date + timedelta(days=1)

    return {
        'message': 'I uploaded a file to s3 (well that is supposed to be happened).',
        'date_last': end_date.strftime('%Y-%m-%dT%H:%M:%S'),
    }

def query_api(request_string):
    try:
        data = requests.get(request_string)
        data.raise_for_status()
        return data
    except requests.exceptions.HTTPError as err:
        logging.warning(err)


def extract_airquality_data(begin_date, end_date):
    def get_airquality_string(page):
        return f'{ENDPOINT_BASE}/measurements?page={page}&order_by=timestamp_measured&order_direction=desc&end={end_date}&start={begin_date}'

    first_page_data = query_api(get_airquality_string(1))
    if first_page_data:
        all_pages_json = {'data': []}
        first_page_json = first_page_data.json()
        last_page = first_page_json['pagination']['last_page']

        for page_nr in range(last_page):
            logging.info('Performing API call...')

            page_data = query_api(get_airquality_string(page_nr + 1))
            if page_data:
                json_data = page_data.json()
                all_pages_json['data'].extend(json_data['data'])
        return json.dumps(all_pages_json)
    else:
        logging.warning('Something went wrong getting the first page data')
        return None

def extract_component_data():
    filename = f'{DATA_PATH}components-airquality-extract.json'
    component_data = query_api(f'{ENDPOINT_BASE}/components?page=1&order_by=formula').json()['data']
    with open(filename, 'w') as file:
        json.dump(component_data, file)
    logging.info(f'Saving file components...')


def extract_stations():
    station_data = query_api(f'{ENDPOINT_BASE}/stations?order_by=&organisation_id=&page=').json()['data']
    filename = f'{DATA_PATH}stations-airquality-extract.json'
    with open(filename, 'w') as file:
        json.dump(station_data, file)
    logging.info(f'Saving file stations ...')

def get_station_ids():
    with open('./data/stations-airquality-extract.json') as json_file:
        data = json.load(json_file)
        df = pd.DataFrame(data)
        station_ids = df['number'].tolist()
        return station_ids

def extract_station_specifics():
    station_ids = get_station_ids()
    station_data = []
    for st_id in station_ids:
        result = query_api(f'{ENDPOINT_BASE}/stations/{st_id}').json()['data']
        station_data.append(result)

    filename = f'{DATA_PATH}stations-specifics-airquality-extract.json'
    with open(filename, 'w') as file:
        json.dump(station_data, file)
    logging.info(f'Saving file stations specifics...')

handler({'date_last': '2017-01-01T01:00:00'}, None)
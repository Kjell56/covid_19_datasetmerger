# read the time series data about corona infections and deaths
import pandas as pd

"""
These scripts have been created by Paul and already documented properly
"""


def read_rename_data():
    data = pd.read_csv('data/kaggle/comp_data/covid19-global-forecasting-week-5/train.csv')
    # rename country column to 'Country'
    data = data.rename(columns={'Country_Region': 'Country', 'ConfirmedCases': 'Infections'})

    # rename 'US' to 'United States'
    data['Country'] = data['Country'].str.replace('US', 'United States')

    # read the data about population by country
    data_pop = pd.read_csv("data/kaggle/comp_data/population_by_country.csv")
    data_pop = data_pop.rename(columns={'Country (or dependent territory)': 'Country'})
    data_pop.head()

    # read data about age-distribution per country
    data_age = pd.read_csv("data/kaggle/comp_data/age_per_country.csv")
    data_age = data_age.rename(columns={'Country': 'Country'})
    data_age.head()

    return data, data_age, data_pop


def clean_data(data, data_age, data_pop):
    # clean additonal data and clean country names, if needed
    data['Country'] = data['Country'].str.replace(r" \(.*\)", "").str.replace("*", "")

    # clean country names for population data
    data_pop['Country'] = data_pop['Country'].str.replace(r'\[.\]', '').str.replace('\s\(.*\)', '')

    # convert percentage (%) columns to numeric by removing the '%' token and casting the rest to float
    # also divide by 100 so that percentage values are on a [0-1] scale
    data_pop['% of worldpopulation'] = data_pop['% of worldpopulation'].map(lambda x: x.split('%')[0]).astype(
        float) / 100

    data_age['age 0 to 14 years[1]'] = data_age['age 0 to 14 years[1]'].map(lambda x: x.split('%')[0]).astype(
        float) / 100
    data_age['age 15 to 64 years[2]'] = data_age['age 15 to 64 years[2]'].map(lambda x: x.split('%')[0]).astype(
        float) / 100
    data_age['age over 65 years[3]'] = data_age['age over 65 years[3]'].map(lambda x: x.split('%')[0]).astype(
        float) / 100

    # convert 'Population' to numeric by removing the ',' token and casting to float
    data_pop['Population'] = data_pop['Population'].str.replace(',', '').astype(float)

    return data, data_age, data_pop


def merge_data(data, data_age, data_pop):
    # merge all data by Country column
    data = pd.merge(data, data_pop, on='Country')
    data = pd.merge(data, data_age, on='Country')

    # rename column names to more appropriate names and drop extra column
    data = data.rename(columns={"age 0 to 14 years[1]": "Age_0_to_14",
                                "age 15 to 64 years[2]": "Age_15_to_64",
                                "age over 65 years[3]": "Age_over_65",
                                'Date_x': 'Date'})

    data = data.drop('Date_y', axis=1)

    # extract month and day from date column
    data['Day'] = data['Date'].str.extract('.*-.*-(.+)').astype(float)

    data['Month'] = data['Date'].str.extract('.-(.+)-.').astype(float)

    # insert 'None' for countries without a Province/State
    data.loc[:, 'Province_State'][data.loc[:, 'Province_State'].isnull()] = "None"

    # Create new ID and remove old one
    data["_id"] = data['Id']
    del data['Id']

    return data

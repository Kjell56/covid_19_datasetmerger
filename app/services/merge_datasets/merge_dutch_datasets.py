import pandas as pd
from app.helpers import logger_helper


class MergeDutchDatasets:
    """
    Class to merge electricity data with the intension to also use other types of functions for other datasets
    TODO: has to be pipelined with pandas
    """
    pd.set_option('display.max_rows', None)
    pd.set_option('display.max_columns', None)

    def __init__(self, data_dir):
        self.data_dir = data_dir + "/"

    def merge_standard_dutch_info(self, df_list):
        """
        Merging the given csv's
        """
        logger_helper.logger("Merging Dutch standard info", 'info', 'merger')
        df1 = pd.read_csv(self.data_dir + df_list[0])
        df2 = pd.read_csv(self.data_dir + df_list[1])
        df3 = pd.read_csv(self.data_dir + df_list[2])

        df1 = self.__rename_standard_columns(df1, 'total_infections')
        df2 = self.__rename_standard_columns(df2, 'total_deceased')
        df3 = self.__rename_standard_columns(df3, 'total_hospitalized')

        df1['measurement_date'] = pd.to_datetime(df1['measurement_date'])
        df2['measurement_date'] = pd.to_datetime(df2['measurement_date'])
        df3['measurement_date'] = pd.to_datetime(df3['measurement_date'])

        merged_df1 = pd.merge(df1, df2, how="outer", on="measurement_date")
        merged_df2 = pd.merge(merged_df1, df3, how="outer", on="measurement_date")
        return merged_df2

    # TODO: merge other more detailed data
    def merge_dutch_non_standard(self, merged_df_stand, non_standard_df_list):
        """
        Merging the standard and non-standard lists of data
        merged_df_stand:
            Contains all data to be expected from CoronaNL and can be merged easily
        non_standard_df_list:
            Contains data which is not usual and needs more work
        :return:
        """
        sex_df = pd.read_csv(self.data_dir + non_standard_df_list[0])
        age_df = pd.read_csv(self.data_dir + non_standard_df_list[1])

        sex_df = sex_df.rename(
            columns={"Datum": "measurement_date", "Geslacht": "gender", "Type": "type", "Aantal": "total"})
        age_df = age_df.rename(
            columns={"Datum": "measurement_date", "LeeftijdGroep": "age_group", "Type": "type", "Aantal": "total"})

        # sex_df['sex'] = sex_df['gender']
        sex_df['male_info'] = sex_df[['type', 'total']].values.tolist()
        # age_df['age'] = age_df[['age_group', 'type', 'total']].values.tolist()

        sex_df['measurement_date'] = pd.to_datetime(sex_df['measurement_date'])
        # age_df['measurement_date'] = pd.to_datetime(age_df['measurement_date'])

        sex_df = sex_df[['measurement_date', 'male_info']]
        # age_df = age_df[['measurement_date', 'age']]

        merged_df3 = pd.merge(merged_df_stand, sex_df, how="left", on="measurement_date")
        # merged_df4 = pd.merge(merged_df3, age_df, how="outer", on="measurement_date")
        return merged_df3

    def merge_dutch_electricity(self, merged_df_stand, electricity_daily_df):
        """
        Merge the standard data and daily data from CoronaNL and electricity data
        merged_df_stand:
            CoronaNL data
        electricity_daily_df:
            Electricity data
        """
        logger_helper.logger("Merging ", 'info', 'Merging Electricity data')

        # rename and reformat data
        # df_electricity = self.reformat_data(df_electricity)

        # merge all data together
        merged_set = pd.merge(merged_df_stand, electricity_daily_df, how="inner", left_on="measurement_date",
                              right_on="electricity_load_date")

        merged_set = self.__add_id_information_electricity(merged_set)
        # del merged_set['time']

        logger_helper.logger("Merging successful", 'info', 'merger')
        return merged_set

    @staticmethod
    def __add_id_information_electricity(merged_set):
        """
        Special function to add electricity specifics
        merged_set:
            The merged dataset
        """
        merged_set_new = merged_set.set_index("electricity_load_date")
        merged_set_new = merged_set_new.sort_index()

        # Create new date column for plotting
        merged_set_new['date'] = merged_set_new.index.date.astype(str)

        # # Special check if electricitydata does not have the latest data yet
        # if merged_set_new['date'].iloc[-1] == "NaT":
        #     ym = time.strftime("%Y-%m-%d")  # get current date
        #     merged_set_new["date"].iloc[-1] = ym

        # Add _id field
        merged_set_new['_id'] = merged_set_new.index

        merged_set_new = merged_set_new.set_index("_id")

        return merged_set_new

    @staticmethod
    def __rename_standard_columns(df, total_name):
        """
        Helper function for renaming the standard columns all together
        """
        df = df.rename(columns={"Datum": "measurement_date", "Aantal": total_name})

        return df

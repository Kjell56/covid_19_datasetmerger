import urllib.request, json
import requests
import os
import logging
import os.path

from app.helpers import logger_helper


class GITData:
    """
    Class for getting data from git repositories
    """
    headers = {'Authorization': 'token ad0f35d3b4dd04d9a46db753e1460404ad9c85c2'}

    def __init__(self, user_name="", repo_name="", data_dir="", get_data=1):
        """
        user_name:
        repo_name:
        data_dir:
        get_data:
            Important if there is no root directory yet, should be 1
            TODO: better coding regarding this workaround
        """
        self.__login_api(GITData.headers)

        self.user_name = user_name
        self.repo_name = repo_name
        self.data_dir = data_dir

        if get_data == 1:
            self.__create_root_dir()

    def download_all_data(self):
        """
        All data will be downloaded from the repo and will be saved in the directory given earlier
        :return:
        """
        logger_helper.logger("loading git data", 'info', 'git')
        url = "https://api.github.com/repos/" + self.user_name + "/" + self.repo_name + "/contents/" + self.data_dir
        data = self.__get_json_data(url)
        if data:
            logger_helper.logger("data successfully downloaded", 'info', 'git')
        else:
            logger_helper.logger("data could not be downloaded", 'error', 'git')
        self.__get_all_data(data, self.rootdir)

    def __create_root_dir(self):
        """
        Create root dir if required to get data instead of commiting a new file
        :return:
        """
        logger_helper.logger("creating root directory", 'info', 'git')
        self.rootdir = "data/git/" + self.repo_name + "/"

        if not os.path.exists(self.rootdir):
            os.mkdir(self.rootdir)
            logger_helper.logger("root directory successfully created at: " + self.rootdir, 'info', 'git')
        else:
            logger_helper.logger("could not create directory at: " + self.rootdir, 'warning', 'git')

    @staticmethod
    def __login_api(headers):
        """
        Logging into github API
        headers:
            Should contain login info
        :return:
        """
        logger_helper.logger("trying to log-in", 'info', 'git')
        login = requests.get('https://api.github.com/user', headers=headers)
        if login:
            logger_helper.logger("successfully logged in ", 'info', 'git')
            logger_helper.logger(login.json(), 'info', 'git')
        else:
            logger_helper.logger("could not login...", 'error', 'git')

    @staticmethod
    def __get_json_data(url):
        """
        Get all json data from URL and decode it
        url:
            url where data resides
        :return:
        """
        with urllib.request.urlopen(url) as url:
            data = json.loads(url.read().decode())
            return data

    def __get_all_data(self, data, path):
        """
        Get all data from GIT and put it into JSON. This function is recursive call and tries to fill up every directory
        data:
            The data downloaded
        path:
            New updated path file per data entry
        :return:
        """
        logger_helper.logger("getting data...", 'info', 'git')
        for i in data:
            logger_helper.logger("getting data: " + i['name'], 'info', 'git')
            if i['type'] == "file":
                logger_helper.logger("file " + i['name'] + " is file", 'info', 'git')
                if os.path.isfile(path):
                    logger_helper.logger("file " + path + " already exists", 'warning', 'git')
                else:
                    logger_helper.logger("file " + path + " has been created", 'info', 'git')
                    urllib.request.urlretrieve(i['download_url'], path + i['name'])
            else:
                if os.path.exists(self.rootdir + i['path']):
                    logger_helper.logger("dir: " + self.rootdir + i['path'] + " already exists", 'warning', 'git')
                else:

                    os.mkdir(self.rootdir + i['path'])
                    path = self.rootdir + i['path'] + "/"
                    logging.info("Created directory at: " + self.rootdir + i['path'] + "/")
                    logger_helper.logger("created directory at: " + self.rootdir + i['path'] + "/", 'info', 'git')

                data = self.__get_json_data(i['url'])
                self.__get_all_data(data, path)

from app.helpers import logger_helper
from kaggle.api.kaggle_api_extended import KaggleApi


class KaggleData:
    """
    Kaggle data can easily be download via the API
    """
    def __init__(self):
        self.__connect_api()

    def __connect_api(self):
        """
        Authenticate with the JSON authentication file
        :return:
        """
        self.api = KaggleApi()
        self.api.authenticate()
        logger_helper.logger("connected to API", 'info', 'kaggle')

    # Signature: dataset_download_file(dataset, file_name, path=None, force=False, quiet=True)
    def download_dataset(self, dataset, file_name, path):
        """
        Download only one dataset
        dataset:
            Dataset to be downloaded
        file_name:
            Name of the dataset to be downloaded
        path:
            Path of the resulting directory in which it will be downloaded
        """
        self.api.dataset_download_file(dataset, file_name, path)
        logger_helper.logger("Downloaded Dataset", 'info', 'kaggle')

    # Signature: dataset_download_files(dataset, path=None, force=False, quiet=True, unzip=False)
    def download_all_dataset(self, dataset, path):
        """
        Download all datasets from challenge
        :return:
        """
        self.api.dataset_download_files(dataset, path)
        logger_helper.logger("Downloaded all datasets", 'info', 'kaggle')

    # Signature: competition name and output path
    def download_comp_dataset(self, competition, path):
        """
        Download a competition dataset
        competition:
            Name of the competition
        :return:
        """
        self.api.competition_download_files(competition, path)
        logger_helper.logger("Downloaded all competition datasets", 'info', 'kaggle')
#!/bin/bash

# Get servers list:
set - f

#Variables from GitLab server:
# Note: They can’t have spaces!!
string=$DEPLOY_SERVER
password=$REGISTRY_PASS
array=(${string//,/ })
# Iterate servers for deploy and pull last commit
# Careful with the ; https://stackoverflow.com/a/20666248/1057052
for i in "${!array[@]}"; do
  echo "Deploy project on server ${array[i]}"
ssh ubuntu@${array[i]} "docker login registry.gitlab.com -u kjell56 -p ${password} && docker stop datasetmerger ; docker pull registry.gitlab.com/kjell56/covid_19_datasetmerger:latest ; docker run --rm --name datasetmerger -d registry.gitlab.com/kjell56/covid_19_datasetmerger:latest"

done
FROM python:3

ADD requirements.txt /app/

WORKDIR /app

ADD . /app

RUN pip install -r requirements.txt

RUN mkdir -p ~/.kaggle/

RUN touch ~/.kaggle/kaggle.json

RUN echo '{"username":"kjellzijlemaker","key":"7a43f09af6cb880fcd8abbfbd25f37ec"}' > ~/.kaggle/kaggle.json

CMD [ "python", "./data_center.py" ]



